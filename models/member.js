const mongoose = require('mongoose');
const mongoosPaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
    _name: String,
    _lastName: String,
    _address: {
        street: String,
        number: Number,
        zip: Number,
        city: String,
        state: String,
        country: String
    },
    _phone: String,
});

class Member {
    constructor(name, lastName, address, phone) {
        this._name = name;
        this._lastName = lastName;  
        this._address = address;
        this._phone = phone;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get lastName() {
        return this._lastName;
    }

    set lastName(value) {
        this._lastName = value;
    }

    get address() {
        return this._address;
    }

    set address(value) {
        this._address = value;
    }

    get phone() {
        return this._phone;
    }   

    set phone(value) {
        this._phone = value;
    }
}

schema.loadClass(Member);
schema.plugin(mongoosPaginate); 
module.exports = mongoose.model('Member', schema);