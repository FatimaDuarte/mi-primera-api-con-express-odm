const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
    _date: Date,
    _member: { 
        type: mongoose.Schema.ObjectId, 
        ref: 'Member' 
    },
    _copy: { 
        type: mongoose.Schema.ObjectId, 
        ref: 'Copy' 
    }
});

class Booking {
    constructor(date, member, copy) {
        this._date = date;
        this._member = member;
        this._copy = copy;
    }

    get date() {
        return this._date;
    }

    set date(value) {
        this._date = value;
    }

    get member() {
        return this._member;
    }

    set member(value) {
        this._member = value;
    }

    get copy() {
        return this._copy;
    }

    set copy(value) {
        this._copy = value;
    }
}

schema.loadClass(Booking);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('Booking', schema);