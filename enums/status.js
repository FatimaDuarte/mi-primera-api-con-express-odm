const Status = Object.freeze({
    AVALIABLE: 'AVALIABLE',
    RENTED: 'RENTED'
});