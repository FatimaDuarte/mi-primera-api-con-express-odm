const Format = Object.freeze({
    VHS: 'VHS',
    DVD: 'DVD',
    BLURAY: 'BLURAY'
});