const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const {expressjwt} = require('express-jwt');

const jwtSecret = "0c69b98ab8c042b43c74e045a8f1d6fa";

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const directorsRouter = require('./routes/directors');
const genresRouter = require('./routes/genres');
const moviesRouter = require('./routes/movies');
const membersRouter = require('./routes/members');
const actorsRouter = require('./routes/actors');
const copiesRouter = require('./routes/copies');
const bookingsRouter = require('./routes/bookings');
const awaitListsRouter = require('./routes/awaitLists')

const app = express();

const url = "mongodb://localhost:27017/video-club";
mongoose.connect(url);

const db = mongoose.connection;
db.on('open', ()=> {
  console.log("Conexion ok");
});

db.on('error', ()=> {
  console.log("Error de conexion");  
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(expressjwt({secret: jwtSecret, algorithms: ['HS256']}).unless({path: ['/login']}));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/directors', directorsRouter);
app.use('/genres', genresRouter);
app.use('/movies', moviesRouter);
app.use('/members', membersRouter);
app.use('/actors', actorsRouter);
app.use('/copies', copiesRouter);
app.use('/bookings', bookingsRouter);
app.use('/awaitlists', awaitListsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
