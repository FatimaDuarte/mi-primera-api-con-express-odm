# API con express ODM

### Prerequisites

Tener:
* Node.js
* NPM
* Express.js
* Docker

## Getting Started

1. Clonar el repositorio

   $ git clone -b ODM git@gitlab.com:FatimaDuarte/mi-primera-api-con-express-odm.git

2. Abrir una terminal en la raíz del repositorio

3. Ejecutar $ npm install

4. Asegurarse de tener un contenedor de mongo ejecutandose
   
5. Ejecutar comando(s):
   
   $ npm run dev | $ npm start

6. Abrir navegador y en la barra de navegación poner el url:

   http://localhost:3000

## Built With

* Javascript - Lenguaje de programación
* Express js - Framework
* Postman - Plataforma API
* Visual Studio Code - Editor de texto
* Node js - Entorno en tiempo de ejecución
* Mongoose - ODM


## Authors

Fátima Monserrath Duarte Pérez 353324.