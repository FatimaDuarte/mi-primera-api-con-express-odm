const express = require('express');
const Copy = require('../models/copy');
const Movie = require('../models/movie');

async function create(req, res, next) {
    const { number, format, movieId, status } = req.body;
    
    let movie = await Movie.findOne({ _id: movieId });

    let copy = new Copy({ number, format, movie, status });
    copy.save().then(obj => res.status(200).json({
        msg: "Copia guardada",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo crear la copia",
        obj: ex
    }));
}

function list(req, res, next) {
    let page = req.params.page ? req.params.page: 1;
    const options = {
        page,
        limit: 5,
        populate: "_movie"
    };

    Copy.paginate({}, options).then(obj => res.status(200).json({
        msg: "Lista de copias: ",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo listar las copias",
        obj: ex
    }));
}   

function index(req, res, next){
    const { id } = req.params;
    Copy.findOne({ _id: id }).populate("_movie").then(obj => res.status(200).json({
        msg: `Copy con id ${id} `,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: `No se encontro copy ${id}`,
        obj: ex
    }));
}

function replace(req, res, next){
    const { id } = req.params;
    let number = req.body.number ? req.body.number: "";
    let format = req.body.format ? req.body.format: "";
    let movie = req.body.movie ? req.body.movie: "";
    let status = req.body.status ? req.body.status: "";

    let copy = new Object({
        _number: number,
        _format: format,
        _movie: movie,
        _status: status
    });

    Copy.findOneAndUpdate({ _id: id }, copy).then(obj => res.status(200).json({
        msg: `Copy con id ${id} reemplazada`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: `No se pudo reemplazar la copy ${id}`,
        obj: ex
    }));
}

function update(req, res, next) {
    const { id } = req.params;
    const { number, format, movie, status } = req.body;

    let copy = new Object({
        _number: number,
        _format: format,
        _movie: movie,
        _status: status
    });

    Copy.findOneAndUpdate({ _id: id }, copy).then(obj => res.status(200).json({
        msg: `Copy con id ${id} actualizada`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: `No se pudo actualizar la copy ${id}`,
        obj: ex
    }));
}

function destroy(req, res, next) {
    const { id } = req.params;

    Copy.findOneAndDelete({ _id: id }).then(obj => res.status(200).json({
        msg: `Copy con id ${id} eliminada`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: `No se pudo eliminar copia ${id}`,
        obj: ex
    }));
}

module.exports = {
    create,
    list,
    index,
    replace,
    update,
    destroy
}