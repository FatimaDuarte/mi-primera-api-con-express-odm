const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../models/user');

function home (req, res, next) {
    res.render('index', { title: 'Express' }); 
}

function login(req, res, next) {
    const { email, password } = req.body;
    const jwtSecret = "0c69b98ab8c042b43c74e045a8f1d6fa";

    User.findOne({"_email":email}).then(user => {
        if(user){
            bcrypt.hash(password, user.salt, (err, hash) => {
                if(err){
                    res.status(403).json({
                        msg: "Usuario o contraseña incorrectos",
                        obj: err
                    });
                }
                if(hash === user.password){
                    res.status(200).json({
                        msg: "Login Ok",
                        obj: jwt.sign({
                            data: user.data, 
                            exp: Math.floor(Date.now()/1000)+60*60},
                            jwtSecret   
                        )
                    });
                } else {
                    res.status(403).json({
                        msg: "Usuario o contraseña incorrectos",
                        obj: null
                    });
                }
            });
        } else {
            res.status(403).json({
                msg: "Usuario o contraseña incorrectos",
                obj: null
            });
        }
    }).catch(ex => res.status(403).json({
        msg: "Usuario o contraseña incorrectos",
        obj: ex
    }));
}

module.exports = {
    home, 
    login
}