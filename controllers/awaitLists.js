const express = require('express');
const AwaitList = require('../models/awaitList.model');
const Member = require('../models/member.model');
const Movie = require('../models/movie.model');

async function create(req, res, next) {
    const { memberId, movieId } = req.body;

    let member = await Member.findOne({ _id: memberId });
    let movie = await Movie.findOne({ _id: movieId });

    let awaitList = new AwaitList({ member, movie });

    awaitList.save().then(obj => res.status(200).json({
        msg: "AwaitList creada",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo crear la AwaitList",
        obj: ex
    }));
}

function list(req, res, next) {
    let page = req.params.page ? req.params.page: 1;
    const options = {
        page,
        limit: 5,
        populate: "_member _movie"
    };

    AwaitList.paginate({}, options).then(obj => res.status(200).json({
        msg: "Awaitlist: ",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo listar AwaitList",
        obj: ex
    }));
}

function index(req, res, next) {
    const { id } = req.params;

    AwaitList.findOne({ _id: id }).populate("_member _movie").then(obj => res.status(200).json({
        msg: `AwaitList con id ${id} `,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: `No se encontro la AwaitList ${id}`,
        obj: ex
    }));
}

function replace(req, res, next) {
    const { id } = req.params;
    let member = req.body.member ? req.body.member: "";
    let movie = req.body.movie ? req.body.movie: "";

    

    let awaitList = new Object({
        _member: member,
        _movie: movie
    });

    AwaitList.findOneAndUpdate({"_id": id}, awaitList, {new: true}).then(obj => res.status(200).json({
        msg:`AwaitList reemplazada `,
    obj:obj})).catch(ex => res.status(500).json({
        msg:`No se pudo reemplazar la AwaitList ${id}`,
    obj:ex}));
}

function update(req, res, next) {
    const { id } = req.params;
    const { member, movie } = req.body;

    let awaitList = new Object();

    if(member) awaitList._member = member;
    if(movie) awaitList._movie = movie;

    AwaitList.findOneAndUpdate({"_id": id}, movie, {new: true}).then(obj => res.status(200).json({
        msg:`AwaitList actualizada `,obj:obj})).catch(ex => res.status(500).json({
            msg:`No se pudo actualizar AwaitList ${id}`,obj:ex}));
}

function destroy(req, res, next) {
    const { id } = req.params;

    AwaitList.findOneAndRemove({"_id": id}).then(obj => res.status(200).json({
        msg:`AwaitList eliminada `,obj:obj})).catch(ex => res.status(500).json({
            msg:`No se pudo eliminar AwaitList ${id}`,obj:ex}));
}

module.exports = {
    create,
    list,
    index,
    replace,
    update,
    destroy
}