const express = require('express');
const Actor = require('../models/actor');

function create(req, res, next){
    const { name, lastName } = req.body;
    let actor = new Actor ({
        name, lastName
    });

    actor.save().then(obj => res.status(200).json({
        msg:"Actor guardado",
    obj:obj})).catch(ex => res.status(500).json({
        msg:"Error al crear actor",
    obj:ex}));
}

function list(req, res, next) {
    let page = req.params.page? req.params.page: 1;
    const options = {
        page: page,
        limit: 5
    };

    Actor.paginate({}, options).then(obj => res.status(200).json({
        msg:"Lista de Actores: ",
    obj:obj})).catch(ex => res.status(500).json({
        msg:"No se pudo listar los actores",
    obj:ex}));
}

function index(req, res, next){
    const { id } = req.params;

    Actor.findOne({"_id":id}).then(obj => res.status(200).json({
        msg:`Actor con id ${id} `,
    obj:obj})).catch(ex => res.status(500).json({
        msg:`No se encontro el Actor ${id}`,
    obj:ex}));
}

function replace(req, res, next){
    const { id } = req.params;
    let name = req.body.name ? req.body.name: "";
    let lastName = req.body.lastName ? req.body.lastName: "";

    let actor = new Object({
        _name: name,
        _lastName: lastName
    });

    Actor.findOneAndUpdate({"_id": id}, actor, {new: true}).then(obj => res.status(200).json({
        msg:`Actor reemplazado `,
    obj:obj})).catch(ex => res.status(500).json({
        msg:`No se pudo reemplazar al Actor ${id}`,
    obj:ex}));
}

function update(req, res, next){
    const { id } = req.params;
    let { name, lastName } = req.body;

    let actor = new Object();

    if(name) actor._name = name;
    if(lastName) actor._lastName = lastName;

    Actor.findOneAndUpdate({"_id":id, director: director}).then(obj => res.status(200).json({
        msg:`Actor actualizado `,
    obj:obj})).catch(ex => res.status(500).json({
        msg:`No se pudo Actualizar el Actor ${id}`,
    obj:ex}));}

function destroy(req, res, next){
    const { id } = req.params;

    Actor.findOneAndDelete({"_id":id}).then(obj => res.status(200).json({
        msg:`Actor borrado `,
    obj:obj})).catch(ex => res.status(500).json({
        msg:`No se pudo borrar el Actor ${id}`,
    obj:ex}));
}

module.exports = {
    create, 
    list, 
    index, 
    replace,
    update, 
    destroy
};
