const express = require('express');
const Genre = require('../models/genre');

function create(req, res, next) {
    const { description } = req.body;
    let genre = new Genre ({
        description
    });

    genre.save().then(obj => res.status(200).json({
        msg:"Genre guardado",
    obj:obj})).catch(ex => res.status(500).json({
        msg:"No se pudo crear genre",
    obj:ex}));
}

function list(req, res, next) {
    let page = req.params.page ? req.params.page: 1;
    const options = {
        page,
        limit: 5
    };

    Genre.paginate({}, options).then(obj => res.status(200).json({
        msg: "Lista de Genres",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudieron listar los Genres",
        obj: ex
    }));
}

function index(req, res, next) {
    const { id } = req.params;
    Genre.findOne({"_id":id}).then(obj => res.status(200).json({
        msg:`Genre con id ${id} `,obj:obj})).catch(ex => res.status(500).json({
            msg:`No se encontro genre ${id}`,obj:ex}));

}

function replace(req, res, next) {
    const { id } = req.params;
    let description = req.body.description ? req.body.description: "";

    let genre = new Object({
        _description: description
    });

    Genre.findOneAndUpdate({"_id": id}, genre, {new: true}).then(obj => res.status(200).json({
        msg:`Genre reemplazado `,obj:obj})).catch(ex => res.status(500).json({
            msg:`No se pudo reemplazar genre ${id}`,obj:ex}));
}   

function update(req, res, next) {
    const { id } = req.params;
    const { description } = req.body;

    let genre = new Object();

    if (description) genre._description = description;
    
    Genre.findOneAndUpdate({"_id": id}, genre, {new: true}).then(obj => res.status(200).json({
        msg:`Genre actualizado `,obj:obj})).catch(ex => res.status(500).json({
            msg:`No se pudo actualizar genre ${id}`,obj:ex}));
}

function destroy(req, res, next) {
    const { id } = req.params;
    Genre.findOneAndRemove({"_id": id}).then(obj => res.status(200).json({
        msg:`Genre eliminado `,obj:obj})).catch(ex => res.status(500).json({
            msg:`No se pudo eliminar genre ${id}`,obj:ex}));
}

module.exports = {
    create,
    list,
    index,
    replace,
    update,
    destroy
};