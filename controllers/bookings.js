const express = require('express');
const Booking = require('../models/booking');
const Member = require('../models/member');
const Copy = require('../models/copy');

async function create(req, res, next) {
    const { date, memberId, copyId } = req.body;
    
    let member = await Member.findOne({ _id: memberId });
    let copy = await Copy.findOneAndUpdate({ _id: copyId }, { $set: { _status: "RENTED" } }, { new: true });

    let booking = new Booking({ date: new Date(), member, copy });

    booking.save().then(obj => res.status(200).json({
        msg: "Booking creado",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo crear el booking",
        obj: ex
    }));
}

function list(req, res, next) {
    let page = req.params.page ? req.params.page : 1;
    const options = {
        page: page,
        limit: 5,
        populate: "_member _copy"
    };

    Booking.paginate({}, options).then(obj => res.status(200).json({
        msg: "Lista de Bookings: ",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudierom listar los bookings",
        obj: ex
    }));
}

function index(req, res, next) {
    const { id } = req.params;

    Booking.findOne({ _id: id }).populate("_member _copy").then(obj => res.status(200).json({
        msg: "Booking encontrado",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se encontro el booking",
        obj: ex
    }));
}

function replace(req, res, next) {
    const { id } = req.params;

    let date = req.body.date ? req.body.date : "";
    let member = req.body.member ? req.body.member : "";
    let copy = req.body.copy ? req.body.copy : "";

    let booking = new Object({
        date,
        member,
        copy
    });

    Booking.findOneAndUpdate({"_id": id}, booking, {new: true}).then(obj => res.status(200).json({
        msg:`Booking reemplazado `,
    obj:obj})).catch(ex => res.status(500).json({
        msg:`No se pudo reemplazar el Booking ${id}`,
    obj:ex}));}

function update(req, res, next) {
    const { id } = req.params;
    const { date, member, copy } = req.body;
    
    let booking = new Object();

    if (date) booking.date = date;
    if (member) booking.member = member;
    if (copy) booking.copy = copy;

    Booking.findOneAndUpdate({"_id": id}, booking, {new: true}).then(obj => res.status(200).json({
        msg:`booking actualizado `,obj:obj})).catch(ex => res.status(500).json({
            msg:`No se pudo actualizar el booking ${id}`,obj:ex}));
}

function destroy(req, res, next) {
    const { id } = req.params;

    const booking = Booking.findOne({ _id: id }).populate("_copy");
    Copy.findOneAndUpdate({"_id": booking._copy._id}, { $set: { _status: "AVAILABLE" } }, { new: true })

    Booking.findOneAndRemove({"_id": id}).then(obj => res.status(200).json({
        msg:`Booking eliminado  `,obj:obj})).catch(ex => res.status(500).json({
            msg:`No se pudo eliminar el Booking ${id}`,obj:ex}));
}

module.exports = {
    create,
    list,
    index,
    replace,
    update,
    destroy
}