const express = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/user');

async function create(req, res, next) {
    const { name, lastName, email, password } = req.body;
    let salt = await bcrypt.genSalt(10);
    const passwordHash = await bcrypt.hash(password, salt);

    let user = new User({
        name,
        lastName,
        email,
        password: passwordHash,
        salt
    });

    user.save().then(obj => res.status(200).json({
        msg: "User creado",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo crear el user",
        obj: ex
    }));
}

function list(req, res, next) {
    let page = req.params.page ? req.params.page : 1;
    const options = {
        page: page,
        limit: 5
    };

    User.paginate({}, options).then(obj => res.status(200).json({
        msg: "Lista de Users: ",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudieron listar los users",
        obj: ex
    }));
}

function index(req, res, next) {
    const { id } = req.params;

    User.findOne({ "_id": id }).then(obj => res.status(200).json({
        msg: `User con id ${id} `,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: `No se encontro el user ${id}`,
        obj: ex
    }));
}

function replace(req, res, next) {
    const { id } = req.params;
    let name = req.body.name ? req.body.name : "";
    let lastName = req.body.lastName ? req.body.lastName : "";
    let email = req.body.email ? req.body.email : "";
    let password = req.body.password ? req.body.password : "";

    let user = new Object({
        _name: name,
        _lastName: lastName,
        _email: email,
        _password: password
    });

    User.findOneAndUpdate({ "_id": id }, user, { new: true }).then(obj => res.status(200).json({
        msg: `User reemplazado `,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: `No se pudo reemplazar el user ${id}`,
        obj: ex
    }));
}

function update(req, res, next) {
    const { id } = req.params;
    let { name, lastName, email, password } = req.body;

    let user = new Object();

    if (name) user._name = name;
    if (lastName) user._lastName = lastName;
    if (email) user._email = email;
    if (password) user._password = password;

    User.findOneAndUpdate({ "_id": id }, user, { new: true }).then(obj => res.status(200).json({
        msg: `User actualizado `,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: `No se pudo actualizar el user ${id}`,
        obj: ex
    }));
}

function destroy(req, res, next) {
    const { id } = req.params;

    User.findOneAndDelete({ "_id": id }).then(obj => res.status(200).json({
        msg: `User eliminado `,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: `No se pudo eliminar el user ${id}`,
        obj: ex
    }));
}

module.exports = {
    create,
    list,
    index,
    replace,
    update,
    destroy
}