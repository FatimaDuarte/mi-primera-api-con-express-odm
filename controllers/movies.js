const express = require('express'); 
const Movie = require('../models/movie');
const Director = require('../models/director');
const Genre = require('../models/genre');
const Actor = require('../models/actor');

async function create(req, res, next){
    const { title, genreId, directorId, cast } = req.body;
    
    let director = await Director.findOne({ _id: directorId });
    let genre = await Genre.findOne({ _id: genreId });
    let castArray = await Actor.find({ _id: { $in: cast } });

    let movie = new Movie({title, director, genre, cast: castArray });
    movie.save().then(obj => res.status(200).json({
        msg: "Movie creado",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo crear movie",
        obj: ex
    }));
}   

function list(req, res, next) {
    let page = req.params.page ? req.params.page: 1;
    const options = {
        page,
        limit: 5,
        populate: "_director _genre _cast"
    };

    Movie.paginate({}, options).then(obj => res.status(200).json({
        msg: "Lista de Movies ",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudieron listar las movies",
        obj: ex
    }));
}

function index(req, res, next){
    const { id } = req.params;
    Movie.findOne({ _id: id }).populate("_director _genre _cast").then(obj => res.status(200).json({
        msg: `Movie con id ${id} `,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: `No se encontro la movie ${id}`,
        obj: ex
    }));
}

function replace(req, res, next){
    const { id } = req.params;
    let title = req.body.title ? req.body.title: "";
    let genre = req.body.genre ? req.body.genre: "";
    let director = req.body.director ? req.body.director: "";
    let cast = req.body.cast ? req.body.cast: "";

    let movie = new Object({
        _title: title,
        _genre: genre,
        _director: director,
        _cast: cast
    });

    Director.findOneAndUpdate({"_id": id}, movie, {new: true}).then(obj => res.status(200).json({
        msg:`Movie reemplazada`,
    obj:obj})).catch(ex => res.status(500).json({
        msg:`No se pudo reemplazar la Movie ${id}`,
    obj:ex}));
}

function update(req, res, next) {
    const { id } = req.params;
    const { title, genre, director, cast } = req.body;

    let movie = new Object();

    if(title) movie._title = title;
    if(genre) movie._genre = genre;
    if(director) movie._director = director;
    if(cast) movie._cast = cast;

    Movie.findOneAndUpdate({"_id": id}, movie, {new: true}).then(obj => res.status(200).json({
        msg:`Movie actualizada `,obj:obj})).catch(ex => res.status(500).json({
            msg:`No se pudo actualizar la Movie ${id}`,obj:ex}));
}

function destroy(req, res, next){
    const { id } = req.params;

    Movie.findOneAndRemove({"_id": id}).then(obj => res.status(200).json({
        msg:`Movie eliminada `,obj:obj})).catch(ex => res.status(500).json({
            msg:`No se pudo eliminar la Movie ${id}`,obj:ex}));
}

module.exports = {
  list, 
  index, 
  create, 
  replace, 
  update, 
  destroy
};