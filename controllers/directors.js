const express = require('express');
const Director = require('../models/director');

function create(req, res, next){
    const {name, lastName} = req.body;
    let director = new Director ({
        name, lastName
    });

    director.paginate({}, options).then(obj => res.status(200).json({
        msg: "Lista de directores: ",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo crear el director",
        obj: ex
    }));
}

function list(req, res, next) {
    let page = req.params.page? req.params.page: 1;
    const options = {
        page: page,
        limit: 5
    };

    Director.paginate({}, options).then(obj => res.status(200).json({
        msg: "Lista de directores: ",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo listar los directores",
        obj: ex
    }));
}

function index(req, res, next){
    const { id } = req.params;
    Director.findOne({"_id":id}).then(obj => res.status(200).json({
        msg:`Director con id ${id} `,
    obj:obj})).catch(ex => res.status(500).json({
        msg:`No se encontro director ${id}`,
    obj:ex}));
    
}

function replace(req, res, next){
    const { id } = req.params;
    let name = req.body.name ? req.body.name: "";
    let lastName = req.body.lastName ? req.body.lastName: ""; // if there is a req.body then lastName = req.body, else lastName = ""

    let director = new Object({
        _name: name,
        _lastName: lastName
    });

    Director.findOneAndUpdate({"_id": id}, director, {new: true}).then(obj => res.status(200).json({
        msg:`Director reemplazado `,
    obj:obj})).catch(ex => res.status(500).json({
        msg:`No se pudo reemplazar al director ${id}`,
    obj:ex}));
}

function update(req, res, next){
    const { id } = req.params;
    let { name, lastName } = req.body;

    let director = new Object();

    if (name) director._name = name;
    if (lastName) director._lastName = lastName;
    
    Director.findOneAndUpdate({"_id":id, director: director}).then(obj => res.status(200).json({
        msg:`Director actualizado `,
    obj:obj})).catch(ex => res.status(500).json({
        msg:`No se pudo actualizar al director ${id}`,
    obj:ex}));
}

function destroy(req, res, next){
    const { id } = req.params;
    Director.findByIdAndRemove({"_id":id}).then(obj => res.status(200).json({
        msg:`Director eliminado`,
    obj:obj})).catch(ex => res.status(500).json({
        msg:`No se pudo eliminar ${id}`,
    obj:ex}));
}

module.exports = {
    create, 
    list, 
    index, 
    replace, 
    update, 
    destroy
};