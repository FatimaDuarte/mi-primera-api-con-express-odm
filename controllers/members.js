const express = require('express');
const Member = require('../models/member');

function create(req, res, next){
    let { name, lastName, phone } = req.body;
    let address = new Object();
    const { street, number, zip, city, state, country } = req.body;
    address = { street, number, zip, city, state, country };    

    let member = new Member({
        name,
        lastName,
        phone,
        address
    });

    member.save().then(obj => res.status(200).json({
        msg:"Member creado",
    obj:obj})).catch(ex => res.status(500).json({
        msg:"No se pudo crear Member",
    obj:ex}));

}

function list(req, res, next) {
    let page = req.params.page ? req.params.page: 1;
    const options = {
        page,
        limit: 5,
        populate: "_address"
    };

    Member.paginate({}, options).then(obj => res.status(200).json({
        msg: "Lista de Members ",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudieron listar los Members",
        obj: ex
    }));
}

function index(req, res, next){
    const { id } = req.params;
    console.log(id)
    Member.findOne({ _id: id }).populate("_address").then(obj => res.status(200).json({
        msg: `Member con id ${id} `,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: `No se encontro el Member ${id}`,
        obj: ex
    }));
}

function replace(req, res, next){
    const { id } = req.params;
    let name = req.body.name ? req.body.name: "";
    let lastName = req.body.lastName ? req.body.lastName: "";
    let phone = req.body.phone ? req.body.phone: "";
    let address = req.body.address ? req.body.address: "";

    let member = new Object({
        _name: name,
        _lastName: lastName,
        _phone: phone,
        _address: address
    });

    Member.findOneAndUpdate({_id: id}, member).then(obj => res.status(200).json({
        msg: `Member con id ${id} reemplazado`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: `No se pudo reemplazar Member ${id}`,
        obj: ex
    }));
}

function update(req, res, next){
    const { id } = req.params;
    const { name, lastName, phone, address } = req.body;

    let member = new Object();

    if(name) member._name = name;
    if(lastName) member._lastName = lastName;
    if(phone) member._phone = phone;
    if(address) member._address = address;

    Member.findOneAndUpdate({_id: id}, member).then(obj => res.status(200).json({ 
        msg:`Member actualizado `,obj:obj})).catch(ex => res.status(500).json({
        msg:`No se pudo actualizar Member ${id}`,obj:ex}));
}

function destroy(req, res, next){
    const { id } = req.params;

    Member.findOneAndRemove({"_id": id}).then(obj => res.status(200).json({
        msg:`Member eliminado `,obj:obj})).catch(ex => res.status(500).json({
            msg:`No se pudo eliminar Member ${id}`,obj:ex}));
}

module.exports = {
  list, 
  index, 
  create, 
  replace, 
  update, 
  destroy
};